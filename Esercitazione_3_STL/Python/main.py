class Ingredient:
    def __init__(self, name: str, description: str, price: int):
        self.Name = name
        self.Description = description
        self.Price = price


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredients)

    def computePrice(self) -> int:
        price = 0
        numIngredients = self.numIngredients()
        for i in range(0, numIngredients):
            price += self.ingredients[i].Price
        return price

class Order:
    def __init__(self, number: int):
        self.Number = number
        self.order = []
        self.__numPizzas: int

    def initializeOrder(self, numPizzas: int):
        self.__numPizzas = numPizzas

    def addPizza(self, pizza: Pizza):
        self.order.append(pizza)

    def numPizzas(self) -> int:
        return len(self.order)

    def getPizza(self, position: int) -> Pizza:
        nPizzas: int = self.numPizzas()
        if position < 1 or position > nPizzas:
            raise ValueError('Position passed is wrong')
        return self.order[position-1]

    def computeTotal(self) -> int:
        total = 0
        nPizzas: int = self.numPizzas()
        for i in range(0, nPizzas):
            total += self.order[i].computePrice()
        return total

class Pizzeria:
    def __init__(self):
        self.elencoIngredienti = []
        self.newIngredient: Ingredient
        self.menuPizzas = []
        self.newPizza: Pizza
        self.orderList = []
        self.newOrder: Order
        self.__numOrder = 999


    def addIngredient(self, name: str, description: str, price: int):
        tmp: Ingredient
        self.newIngredient = Ingredient(name, description, price)

        ingredients: int = len(self.elencoIngredienti)
        for i in range(0, ingredients):
            if self.newIngredient.Name is self.elencoIngredienti[i].Name:
                raise ValueError('Ingredient already inserted')
        self.elencoIngredienti.append(self.newIngredient)
        for i in range(0, ingredients):
            if self.elencoIngredienti[i + 1].Name < self.elencoIngredienti[i].Name:
                tmp = self.elencoIngredienti[i]
                self.elencoIngredienti[i] = self.elencoIngredienti[i + 1]
                self.elencoIngredienti[i + 1] = tmp

    def findIngredient(self, name: str) -> Ingredient:
        ingredients: int = len(self.elencoIngredienti)

        for i in range(0, ingredients):
            if self.elencoIngredienti[i].Name is name:
                return self.elencoIngredienti[i]
        raise ValueError('Ingredient not found')

    def addPizza(self, name: str, ingredients: list = []):
        self.newPizza = Pizza(name)
        numPizzas: int = len(self.menuPizzas)
        for i in range(0, numPizzas):
            if self.newPizza.Name is self.menuPizzas[i].Name:
                raise ValueError('Pizza already inserted')
        pizzaIngredients: int = len(ingredients)
        numIngredients: int = len(self.elencoIngredienti)
        for i in range(0, pizzaIngredients):
            for j in range(0, numIngredients):
                if self.elencoIngredienti[j].Name is ingredients[i]:
                    self.newIngredient = self.elencoIngredienti[j]
                    self.newPizza.addIngredient(self.newIngredient)
        self.menuPizzas.append(self.newPizza)

    def findPizza(self, name: str) -> Pizza:
        numPizzas: int = len(self.menuPizzas)
        for i in range(0, numPizzas):
            if self.menuPizzas[i].Name is name:
                return self.menuPizzas[i]
        raise ValueError('Pizza not found')

    def createOrder(self, pizzas: list = []) -> int:
        nPizzas: int = len(pizzas)
        if nPizzas == 0:
            raise ValueError('Empty order')
        self.newOrder = Order(self.__numOrder)
        self.newOrder.initializeOrder(nPizzas)
        menuSize: int = len(self.menuPizzas)
        for i in range(0, nPizzas):
            for j in range(0, menuSize):
                if pizzas[i] is self.menuPizzas[j].Name:
                    self.newPizza = self.menuPizzas[j]
                    self.newOrder.addPizza(self.newPizza)
        self.newOrder.Number = 1000 + len(self.orderList)
        self.orderList.append(self.newOrder)
        return self.newOrder.Number


    def findOrder(self, numOrder: int) -> Order:
        numOrders: int = len(self.orderList)
        for i in range(0, numOrders):
            if numOrder == self.orderList[i].Number:
                return self.orderList[i]
        raise ValueError('Order not found')

    def getReceipt(self, numOrder: int) -> str:
        receipt = ''
        numOrders: int = len(self.orderList)
        for i in range(0,numOrders):
            if self.orderList[i].Number == numOrder:
                numPizzas: int = self.orderList[i].numPizzas()
                for j in range(1, numPizzas + 1):
                    receipt += "- " + self.orderList[i].getPizza(j).Name + ", " + \
                               str(self.orderList[i].getPizza(j).computePrice()) + " euro" + "\n"
                receipt += "  TOTAL: " + str(self.orderList[i].computeTotal()) + " euro" + "\n"
                return receipt
        raise ValueError('Order not found')

    def listIngredients(self) -> str:
        self.ingredientlist = ""
        ingredients: int = len(self.elencoIngredienti)
        for i in range(0, ingredients):
            self.ingredientlist += self.elencoIngredienti[i].Name + " - '" + self.elencoIngredienti[i].Description + \
                                   "': " + str(self.elencoIngredienti[i].Price) + " euro" + "\n"
        return self.ingredientlist

    def menu(self) -> str:
        menuPizzas = ""
        numPizzas: int = len(self.menuPizzas)

        for i in range(0, numPizzas):
            menuPizzas += self.menuPizzas[i].Name + " (" + str(self.menuPizzas[i].numIngredients()) + \
                          " ingredients): " + str(self.menuPizzas[i].computePrice()) + " euro" + "\n"
        return menuPizzas
