#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
  private:
      vector <Ingredient> Ingredients;
  public:
  string Name;
  //funzione Reset() introdotta per sorvolare problemi con conteggi di ingredienti e somme di prezzi
  void Reset() { Ingredients.clear();}
  void AddIngredient(const Ingredient& ingredient) { Ingredients.push_back(ingredient); }
  int NumIngredients() const { return Ingredients.size(); }
  int ComputePrice() const;
  };

  class Order {
  private:
      vector<Pizza> order;
      int nPizzas;
  public:
  int Number; //introduco l'identificativo in pubblica
  void InitializeOrder(int numPizzas) { nPizzas = numPizzas; }
  void AddPizza(const Pizza& pizza) { order.push_back(pizza);}
  const Pizza& GetPizza(const int& position) const;
  int NumPizzas() const { return order.size(); }
  int ComputeTotal() const;
  };

  class Pizzeria {
  public:
      vector<Ingredient> ingredientslist;
      Ingredient newIngredient;
      vector<Pizza> pizzaslist;
      Pizza newPizza;
      vector<Order> orderlist;
      Order newOrder;

      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const; //in input il nome dell'ingrediente ricercato
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
