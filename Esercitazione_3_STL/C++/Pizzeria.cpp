#include "Pizzeria.h"

namespace PizzeriaLibrary {

//Calcolo prezzo di una pizza sommando il prezzo degli ingredienti
int Pizza::ComputePrice() const
{
    int price = 0;
    int numIngredients = NumIngredients();

    for(int i = 0; i < numIngredients; i++)
    {
        price += Ingredients[i].Price;
    }
    return price;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    Ingredient tmp;
    newIngredient.Name = name;
    newIngredient.Description = description;
    newIngredient.Price = price;
    unsigned int nIngredients = ingredientslist.size();

    //Inserimento nuovo ingrediente

    for(unsigned int i = 0; i < nIngredients; i++)
    {
        if(newIngredient.Name == ingredientslist[i].Name )
        {
            throw runtime_error("Ingredient already inserted");
            break;
        }
    }
    ingredientslist.push_back(newIngredient);

    //Riordinamento lista ingredienti (in ordine alfabetico) post inserimento

    for(unsigned int i = 0; i < nIngredients; i++)
    {
        if(ingredientslist[i+1].Name < ingredientslist[i].Name)
        {
            tmp = ingredientslist[i];
            ingredientslist[i] = ingredientslist[i+1];
            ingredientslist[i+1] = tmp;
        }
    }
}

//Ricerca di un ingrediente all'interno della lista ingredienti
const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    unsigned int numINgredient = ingredientslist.size();
    for (unsigned int i = 0; i < numINgredient; i++)
    {
        if (ingredientslist[i].Name == name)
        {
            return ingredientslist[i];
        }
    }
    throw runtime_error("Ingredient not found");
}

//Aggiunta pizza al menu e lancio di eccezione pizza già presente
void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    newPizza.Reset(); //Funzione reset introdotta per via di errori di conteggio sul calcolo del prezzo
    newPizza.Name = name; //assegnazione nome pizza alla nuova pizza da inserire nel menu
    unsigned int numPizzas = pizzaslist.size();

    for(unsigned int i = 0; i < numPizzas; i++)
    {
        if(newPizza.Name == pizzaslist[i].Name )
        {
            throw runtime_error("Pizza already inserted");
            break;
        }
    }
    unsigned int pizzaIngredients = ingredients.size();
    unsigned int numIngredients = ingredientslist.size();

    //assegno ingredienti trovati dalla lista per nome (in input) e aggiungo newPizza in coda al menu
    for(unsigned int i = 0; i < pizzaIngredients; i++)
    {
        for(unsigned int j = 0; j < numIngredients; j++)
        {
            if (ingredients[i] == ingredientslist[j].Name)
            {
                newPizza.AddIngredient(ingredientslist[j]);
            }
        }
    }
    pizzaslist.push_back(newPizza);

    Pizza tmp;
    //ordinamento pizze nel menu per prezzo crescente
    for(unsigned int i = 0; i < numPizzas; i++)
    {
        if(pizzaslist[i+1].ComputePrice() < pizzaslist[i].ComputePrice())
        {
            tmp = pizzaslist[i];
            pizzaslist[i] = pizzaslist[i+1];
            pizzaslist[i+1] = tmp;
        }
    }
}

//ricerca di una pizza dal menu per nome
const Pizza &Pizzeria::FindPizza(const string &name) const
{
    unsigned int numPizzas = pizzaslist.size();
    for (unsigned int i = 0; i < numPizzas; i++)
    {
        if (pizzaslist[i].Name == name)
        {
            return pizzaslist[i];
        }
    }
    throw runtime_error("Pizza not found");
}

//Creazione di un ordine se l'ordine contiene pizze e aggiunta del suddetto in coda alla lista di ordini
int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    int numPizzas = pizzas.size();
    if(numPizzas == 0)
    {
        throw runtime_error("Empty order");
    }
    newOrder.InitializeOrder(numPizzas);
    unsigned int menusize = pizzaslist.size();
    for (int i = 0; i < numPizzas; i++)
    {
        for (unsigned int j = 0; j < menusize; j++)
        {
            if (pizzas[i] == pizzaslist[j].Name)
            {
                newPizza = pizzaslist[j];
                newOrder.AddPizza(newPizza);
            }
        }
    }
    newOrder.Number = 1000 + orderlist.size();
    orderlist.push_back(newOrder);
    return newOrder.Number;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    unsigned int numorders = orderlist.size();
    for(unsigned int i = 0; i < numorders; i++)
    {
        if(orderlist[i].Number == numOrder)
        {
            return orderlist[i];
        }
    }
    throw runtime_error("Order not found");
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string receipt = "";
    int numOrders = orderlist.size();
    for (int i = 0; i < numOrders; i++)
    {
        if (orderlist[i].Number == numOrder)
        {
            int numPizzas = orderlist[i].NumPizzas();
            for (int j = 1; j <= numPizzas; j++ )
            {
                receipt += "- " + orderlist[i].GetPizza(j).Name + ", " + std::to_string(orderlist[i].GetPizza(j).ComputePrice()) + " euro" + "\n";
            }
            receipt += "  TOTAL: " + std::to_string(orderlist[i].ComputeTotal()) + " euro" + "\n";
            return receipt;
        }
    }
    throw runtime_error("Order not found");
}

string Pizzeria::ListIngredients() const
{
    string IngredientsList = "";
    unsigned int numIngredients = ingredientslist.size();

    for (unsigned int i = 0; i < numIngredients; i++)
    {
        IngredientsList += ingredientslist[i].Name + " - '" + ingredientslist[i].Description + "': " + std::to_string(ingredientslist[i].Price) + " euro" + "\n";
    }
    return IngredientsList;
}

string Pizzeria::Menu() const
{
    string PizzasList = "";
    unsigned int numPizzas = pizzaslist.size();

    for (unsigned int i = 0; i < numPizzas; i++)
    {
        PizzasList += pizzaslist[i].Name + " (" + std::to_string(pizzaslist[i].NumIngredients()) + " ingredients): " + std::to_string(pizzaslist[i].ComputePrice()) + " euro" + "\n";
    }
    return PizzasList;
}

const Pizza &Order::GetPizza(const int &position) const
{
    int npizzas = order.size();
    if(position > npizzas)
    {
        throw runtime_error("Position passed is wrong");
    }
    else
    {
        return order[position-1];
    }
}

int Order::ComputeTotal() const
{
    int totale = 0;
    int numPizzas = NumPizzas();
    for (int i = 0; i < numPizzas; i++)
    {
        //sommo i prezzi di ogni pizza (calcolato come somma degli ingredienti di ognuna tramite ComputePrice()
        totale += order[i].ComputePrice();
    }
    return totale;
}

}
