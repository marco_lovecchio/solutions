from unittest import TestCase

from src.Intersection import Intersection
import src.Point as Point
from src.Enumerations import Type


class TestIntersection(TestCase):

    def test_SetFirstSegment(self):
        a = Point.Point(0.0, 0.0)
        b = Point.Point(3.0, 0.0)
        intersection = Intersection()
        intersection.SetFirstSegment(a, b)

        try:
            self.assertEqual(intersection.Origin[0], a.X)
            self.assertEqual(intersection.Origin[1], a.Y)
            self.assertEqual(intersection.Last[0], b.X)
            self.assertEqual(intersection.Last[1], b.Y)
            self.assertEqual(intersection.matrixTangentVector[0, 0], (intersection.Last - intersection.Origin)[0])
            self.assertEqual(intersection.matrixTangentVector[1, 0], (intersection.Last - intersection.Origin)[1])
            self.assertEqual(intersection.Tangent1[0], 1.0)
            self.assertEqual(intersection.Tangent1[1], 0.0)
            self.assertEqual(intersection.originFirstSegment[0], a.X)
            self.assertEqual(intersection.originFirstSegment[1], a.Y)
        except Exception as ex:
            self.fail()

    def test_SetSecondSegment(self):
        a = Point.Point(0.0, 0.0)
        b = Point.Point(3.0, 0.0)
        c = Point.Point(0.0, 2.0)
        d = Point.Point(6.0, 2.0)
        intersection = Intersection()
        intersection.SetFirstSegment(a, b)
        intersection.SetSecondSegment(c, d)

        try:
            self.assertEqual(intersection.Origin[0], c.X)
            self.assertEqual(intersection.Origin[1], c.Y)
            self.assertEqual(intersection.Last[0], d.X)
            self.assertEqual(intersection.Last[1], d.Y)
            self.assertEqual(intersection.matrixTangentVector[0, 1], (intersection.Origin - intersection.Last)[0])
            self.assertEqual(intersection.matrixTangentVector[1, 1], (intersection.Origin - intersection.Last)[1])
            self.assertEqual(intersection.Tangent2[0], -1.0)
            self.assertEqual(intersection.Tangent2[1], 0.0)
            self.assertEqual(intersection.Normal2[0], 0.0)
            self.assertEqual(intersection.Normal2[1], -1.0)
            self.assertEqual(intersection.rightHandSide[0], 0.0)
            self.assertEqual(intersection.rightHandSide[1], 2.0)
        except Exception as ex:
            self.fail()

    def test_ComputeIntersection(self):
        intersection = Intersection()

        # test segmenti paralleli senza intersezione
        try:
            a = Point.Point(0.0, 0.0)
            b = Point.Point(3.0, 0.0)
            c = Point.Point(0.0, 2.0)
            d = Point.Point(6.0, 2.0)

            intersection.SetFirstSegment(a, b)
            intersection.SetSecondSegment(c, d)

            self.assertFalse(intersection.ComputeIntersection())
            self.assertEqual(intersection.TypeIntersection(), Type.NoIntersection)
        except Exception as ex:
            self.fail()

        # Test segmenti paralleli con intersezione e giacenti sulla stessa retta
        try:
            a = Point.Point(0.0, 0.0)
            b = Point.Point(3.0, 0.0)
            c = Point.Point(1.0, 0.0)
            d = Point.Point(2.0, 0.0)

            intersection.SetFirstSegment(a, b)
            intersection.SetSecondSegment(c, d)

            self.assertTrue(intersection.ComputeIntersection())
            self.assertEqual(intersection.TypeIntersection(), Type.IntersectionParallelOnSegment)
        except Exception as ex:
            self.fail()

        # Test segmenti paralleli che giacciono sulla stessa retta
        try:
            a = Point.Point(0.0, 0.0)
            b = Point.Point(3.0, 0.0)
            c = Point.Point(4.0, 0.0)
            d = Point.Point(6.0, 0.0)

            intersection.SetFirstSegment(a, b)
            intersection.SetSecondSegment(c, d)

            self.assertTrue(intersection.ComputeIntersection())
            self.assertEqual(intersection.TypeIntersection(), Type.IntersectionParallelOnLine)
        except Exception as ex:
            self.fail()

        # Test due segmenti incidenti
        try:
            a = Point.Point(1.0, 0.0)
            b = Point.Point(5.0, 0.0)
            c = Point.Point(3.0, -6.0)
            d = Point.Point(3.0, 6.0)
            p = Point.Point(3.0, 0.0)

            intersection.SetFirstSegment(a, b)
            intersection.SetSecondSegment(c, d)

            self.assertTrue(intersection.ComputeIntersection())
            self.assertEqual(intersection.TypeIntersection(), Type.IntersectionOnSegment)
            self.assertTrue(abs(intersection.IntersectionPoint(a, b).X - p.X) < 1e-8)
            self.assertTrue(abs(intersection.IntersectionPoint(a, b).X - p.X) < 1e-8)

        except Exception as ex:
            self.fail()

        # Test due segmenti non incidenti, giacenti su due rette incidenti
        try:
            a = Point.Point(3, 6)
            b = Point.Point(3, 2)
            c = Point.Point(5, 0)
            d = Point.Point(1, 0)
            p = Point.Point(3.0, 0.0)

            intersection.SetFirstSegment(a, b)
            intersection.SetSecondSegment(c, d)

            self.assertTrue(intersection.ComputeIntersection())
            self.assertEqual(intersection.TypeIntersection(), Type.IntersectionOnLine)
            self.assertTrue(abs(intersection.IntersectionPoint(a, b).X - p.X) < 1e-8)
            self.assertTrue(abs(intersection.IntersectionPoint(a, b).X - p.X) < 1e-8)

        except Exception as ex:
            self.fail()
