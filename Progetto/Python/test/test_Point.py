from unittest import TestCase

import src.Point as Point


class TestPoint(TestCase):

    # constructor test
    def test__init__(self):
        p1 = Point.Point(1.2, 2.4)
        try:
            self.assertEqual(p1.X, 1.2)
            self.assertEqual(p1.Y, 2.4)
        except Exception as ex:
            self.fail()

    # test norm
    def test_ComputeNorm2(self):
        point = Point.Point(3.0, 4.0)

        try:
            self.assertEqual(point.X, 3.0)
            self.assertEqual(point.Y, 4.0)
            self.assertTrue(abs(point.ComputeNorm2() - 5 < 1e-8))
        except Exception as ex:
            self.fail()

    # test sum
    def test__add__(self):
        p1 = Point.Point(1.0, 2.0)
        p2 = Point.Point(2.0, 3.0)
        p3 = p1 + p2

        try:
            self.assertEqual(p3.X, 3.0)
            self.assertEqual(p3.Y, 5.0)
        except Exception as ex:
            self.fail()

    # test subtraction
    def test__sub__(self):
        p1 = Point.Point(1.0, 2.0)
        p2 = Point.Point(2.0, 3.0)
        p3 = p1 - p2

        try:
            self.assertEqual(p3.X, - 1.0)
            self.assertEqual(p3.Y, - 1.0)
        except Exception as ex:
            self.fail()

    # test di operator minore
    def test__lt__(self):
        p1 = Point.Point(1.0, 2.0)
        p2 = Point.Point(2.0, 3.0)
        try:
            self.assertTrue(p1 < p2)
        except Exception as ex:
            self.fail()

    # test operator uguaglianza
    def test__eq__(self):
        p1 = Point.Point(1.0, 2.0)
        p2 = p1

        try:
            self.assertTrue(p1 == p2)
        except Exception as ex:
            self.fail()
