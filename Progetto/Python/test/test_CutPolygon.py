from unittest import TestCase

from src.Cut_Polygon import CutPolygon
import src.Point as Point
from src.Intersection import Intersection
import src.Polygon as Polygon


class TestCutPolygon(TestCase):
    # questa istruzione inizializza il file che contiene lo script di Matlab e cancella eventuali contenuti al suo interno
    file = open("scriptProject.m", "w")
    file.write("%Launching unittests from TestCutPolygon...\n")
    file.close()

    # Triangle

    def test_CutPolygons_Triangle(self):

        try:
            p0 = Point.Point(7.0, 1.0)
            p1 = Point.Point(11.0, 5.0)
            p2 = Point.Point(3.0, 5.0)
            s0 = Point.Point(8.0, 2.0)
            s1 = Point.Point(10.0, 4.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon([p0, p1, p2])
            polygon.Indices = [0, 1, 2]

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(polygon.Vertices, polygon.Indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[3].X - 8.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[3].Y - 2.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[4].X - 10.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[4].Y - 4.0) < 10e-5)
            self.assertEqual(len(cutPolygon.newPolygons), 1)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 3, 4, 1, 2])

            poly = Polygon.Polygon(
                [cutPolygon.newPoints[0], cutPolygon.newPoints[3], cutPolygon.newPoints[4], cutPolygon.newPoints[1],
                 cutPolygon.newPoints[2]])
            poly.SetPolygon(
                [cutPolygon.newPoints[0], cutPolygon.newPoints[3], cutPolygon.newPoints[4], cutPolygon.newPoints[1],
                 cutPolygon.newPoints[2]])
            self.assertTrue(abs(poly.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # Rectangle

    def test_CutPolygons_Rectangle(self):

        try:
            p0 = Point.Point(1.0, 1.0)
            p1 = Point.Point(5.0, 1.0)
            p2 = Point.Point(5.0, 3.1)
            p3 = Point.Point(1.0, 3.1)
            s0 = Point.Point(2.0, 1.2)
            s1 = Point.Point(4.0, 3.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon([p0, p1, p2, p3])
            polygon.Indices = [0, 1, 2, 3]

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(polygon.Vertices, polygon.Indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[4].X - 1.77778) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[4].Y - 1.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[5].X - 2.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[5].Y - 1.2) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[6].X - 4.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[6].Y - 3.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[7].X - 4.11111) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[7].Y - 3.1) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 2)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 4, 5, 6, 7, 3])
            self.assertListEqual(cutPolygon.newPolygons[1], [4, 1, 2, 7, 6, 5])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[4], cutPolygon.newPoints[5], cutPolygon.newPoints[6],
                     cutPolygon.newPoints[7], cutPolygon.newPoints[3]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[4], cutPolygon.newPoints[1], cutPolygon.newPoints[2], cutPolygon.newPoints[7],
                     cutPolygon.newPoints[6], cutPolygon.newPoints[5]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            self.assertTrue(abs(poly1.Area() + poly2.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # test Polygon I concavo

    def test_CutPolygons_Polygon1(self):

        try:
            p0 = Point.Point(1.5, 1.0)
            p1 = Point.Point(5.6, 1.5)
            p2 = Point.Point(5.5, 4.8)
            p3 = Point.Point(4.0, 6.2)
            p4 = Point.Point(3.2, 4.2)
            p5 = Point.Point(1.0, 4.0)
            s0 = Point.Point(2.0, 3.7)
            s1 = Point.Point(4.1, 5.9)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon([p0, p1, p2, p3, p4, p5])
            polygon.Indices = [0, 1, 2, 3, 4, 5]

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(polygon.Vertices, polygon.Indices, segment)
            self.assertTrue(abs(cutPolygon.newPoints[6].X - 4.20433) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[6].Y - 6.00929) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[7].X - 4.1) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[7].Y - 5.9) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[8].X - 3.72131) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[8].Y - 5.50328) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[9].X - 2.40860) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[9].Y - 4.12805) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[10].X - 2.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[10].Y - 3.7) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[11].X - 1.19122) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[11].Y - 2.85270) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 3)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 2, 6, 7, 8, 4, 9, 10, 11])
            self.assertListEqual(cutPolygon.newPolygons[1], [6, 3, 8, 7])
            self.assertListEqual(cutPolygon.newPolygons[2], [9, 5, 11, 10])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[2], cutPolygon.newPoints[6],
                     cutPolygon.newPoints[7], cutPolygon.newPoints[8], cutPolygon.newPoints[4], cutPolygon.newPoints[9],
                     cutPolygon.newPoints[10], cutPolygon.newPoints[11]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[6], cutPolygon.newPoints[3], cutPolygon.newPoints[8], cutPolygon.newPoints[7]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            list3 = [cutPolygon.newPoints[9], cutPolygon.newPoints[5], cutPolygon.newPoints[11],
                     cutPolygon.newPoints[10]]
            poly3 = Polygon.Polygon(list3)
            poly3.SetPolygon(list3)

            self.assertTrue(abs(poly1.Area() + poly2.Area() + poly3.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # Pentagon

    def test_CutPolygons_Pentagon(self):

        try:
            p0 = Point.Point(2.5, 1.0)
            p1 = Point.Point(4.0, 2.1)
            p2 = Point.Point(3.4, 4.2)
            p3 = Point.Point(1.6, 4.2)
            p4 = Point.Point(1.0, 2.1)
            s0 = Point.Point(1.4, 2.75)
            s1 = Point.Point(3.6, 2.2)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon([p0, p1, p2, p3, p4])
            polygon.Indices = [0, 1, 2, 3, 4]

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(polygon.Vertices, polygon.Indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[5].X - 3.6) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[5].Y - 2.2) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[6].X - 1.4) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[6].Y - 2.75) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[7].X - 1.2) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[7].Y - 2.8) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 2)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 5, 6, 7, 4])
            self.assertListEqual(cutPolygon.newPolygons[1], [1, 2, 3, 7, 6, 5])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[5], cutPolygon.newPoints[6],
                     cutPolygon.newPoints[7], cutPolygon.newPoints[4]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[1], cutPolygon.newPoints[2], cutPolygon.newPoints[3], cutPolygon.newPoints[7],
                     cutPolygon.newPoints[6], cutPolygon.newPoints[5]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            self.assertTrue(abs(poly1.Area() + poly2.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # Additional test: intersezione poligono con asse y

    def test_CutPolygons_Additional1(self):

        try:
            p0 = Point.Point(2.0, -2.0)
            p1 = Point.Point(0.0, -1.0)
            p2 = Point.Point(3.0, 1.0)
            p3 = Point.Point(0.0, 2.0)
            p4 = Point.Point(3.0, 2.0)
            p5 = Point.Point(3.0, 3.0)
            p6 = Point.Point(-1.0, 3.0)
            p7 = Point.Point(-3.0, 1.0)
            p8 = Point.Point(0.0, 0.0)
            p9 = Point.Point(-3.0, -2.0)

            vertices = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9]
            indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            s0 = Point.Point(0.0, -3.0)
            s1 = Point.Point(0.0, 4.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon(vertices)
            polygon.Indices = indices

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(vertices, indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[10].X - 0.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[10].Y - 3.0) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[11].X - 0.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[11].Y - -2.0) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 5)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 11])
            self.assertListEqual(cutPolygon.newPolygons[1], [1, 2, 3, 8])
            self.assertListEqual(cutPolygon.newPolygons[2], [3, 4, 5, 10])
            self.assertListEqual(cutPolygon.newPolygons[3], [10, 6, 7, 8, 3])
            self.assertListEqual(cutPolygon.newPolygons[4], [8, 9, 11, 1])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[11]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[1], cutPolygon.newPoints[2], cutPolygon.newPoints[3], cutPolygon.newPoints[8]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            list3 = [cutPolygon.newPoints[3], cutPolygon.newPoints[4], cutPolygon.newPoints[5],
                     cutPolygon.newPoints[10]]
            poly3 = Polygon.Polygon(list3)
            poly3.SetPolygon(list3)

            list4 = [cutPolygon.newPoints[10], cutPolygon.newPoints[6], cutPolygon.newPoints[7],
                     cutPolygon.newPoints[8], cutPolygon.newPoints[3]]
            poly4 = Polygon.Polygon(list4)
            poly4.SetPolygon(list4)

            list5 = [cutPolygon.newPoints[8], cutPolygon.newPoints[9], cutPolygon.newPoints[11],
                     cutPolygon.newPoints[1]]
            poly5 = Polygon.Polygon(list5)
            poly5.SetPolygon(list5)

            self.assertTrue(
                abs(poly1.Area() + poly2.Area() + poly3.Area() + poly4.Area() + poly5.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # Additional test2: intersezione poligono con bisettrice del primo e terzo quadrante

    def test_CutPolygons_Additional2(self):

        try:
            p0 = Point.Point(2.0, -2.0)
            p1 = Point.Point(0.0, -1.0)
            p2 = Point.Point(3.0, 1.0)
            p3 = Point.Point(0.0, 2.0)
            p4 = Point.Point(3.0, 2.0)
            p5 = Point.Point(3.0, 3.0)
            p6 = Point.Point(-1.0, 3.0)
            p7 = Point.Point(-3.0, 1.0)
            p8 = Point.Point(0.0, 0.0)
            p9 = Point.Point(-3.0, -2.0)

            vertices = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9]
            indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            s0 = Point.Point(-4.0, -4.0)
            s1 = Point.Point(4.0, 4.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon(vertices)
            polygon.Indices = indices

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(vertices, indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[10].X - 1.5) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[10].Y - 1.5) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[11].X - 2.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[11].Y - 2.0) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[12].X - -2.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[12].Y - -2.0) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 4)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 2, 10, 8, 12])
            self.assertListEqual(cutPolygon.newPolygons[1], [10, 3, 11, 5, 6, 7, 8])
            self.assertListEqual(cutPolygon.newPolygons[2], [11, 4, 5])
            self.assertListEqual(cutPolygon.newPolygons[3], [8, 9, 12])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[2],
                     cutPolygon.newPoints[10], cutPolygon.newPoints[8], cutPolygon.newPoints[12]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[10], cutPolygon.newPoints[3], cutPolygon.newPoints[11],
                     cutPolygon.newPoints[5], cutPolygon.newPoints[6], cutPolygon.newPoints[7], cutPolygon.newPoints[8]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            list3 = [cutPolygon.newPoints[11], cutPolygon.newPoints[4], cutPolygon.newPoints[5]]
            poly3 = Polygon.Polygon(list3)
            poly3.SetPolygon(list3)

            list4 = [cutPolygon.newPoints[8], cutPolygon.newPoints[9], cutPolygon.newPoints[12]]
            poly4 = Polygon.Polygon(list4)
            poly4.SetPolygon(list4)

            self.assertTrue(abs(poly1.Area() + poly2.Area() + poly3.Area() + poly4.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # test Crown (simplified figure)

    def test_CutPolygons_Crown(self):

        try:
            p0 = Point.Point(1.0, 1.0)
            p1 = Point.Point(8.5, 1.0)
            p2 = Point.Point(8.5, 6.0)
            p3 = Point.Point(7.0, 3.0)
            p4 = Point.Point(6.5, 3.0)
            p5 = Point.Point(5.75, 5.0)
            p6 = Point.Point(5.0, 3.0)
            p7 = Point.Point(4.5, 3.0)
            p8 = Point.Point(3.75, 5.0)
            p9 = Point.Point(3.0, 3.0)
            p10 = Point.Point(2.5, 3.0)
            p11 = Point.Point(1.0, 6.0)

            vertices = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11]
            indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
            s0 = Point.Point(2.0, 3.0)
            s1 = Point.Point(10.0, 3.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon(vertices)
            polygon.Indices = indices

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(vertices, indices, segment)

            self.assertEqual(len(cutPolygon.newPolygons), 5)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 12, 3, 4, 6, 7, 9, 10, 13, 14])
            self.assertListEqual(cutPolygon.newPolygons[1], [12, 2, 3])
            self.assertListEqual(cutPolygon.newPolygons[2], [4, 5, 6])
            self.assertListEqual(cutPolygon.newPolygons[3], [7, 8, 9])
            self.assertListEqual(cutPolygon.newPolygons[4], [10, 11, 14, 13])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[12],
                     cutPolygon.newPoints[3], cutPolygon.newPoints[4], cutPolygon.newPoints[6],
                     cutPolygon.newPoints[7], cutPolygon.newPoints[9], cutPolygon.newPoints[10],
                     cutPolygon.newPoints[13], cutPolygon.newPoints[14]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[12], cutPolygon.newPoints[2], cutPolygon.newPoints[3]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            list3 = [cutPolygon.newPoints[4], cutPolygon.newPoints[5], cutPolygon.newPoints[6]]
            poly3 = Polygon.Polygon(list3)
            poly3.SetPolygon(list3)

            list4 = [cutPolygon.newPoints[7], cutPolygon.newPoints[8], cutPolygon.newPoints[9]]
            poly4 = Polygon.Polygon(list4)
            poly4.SetPolygon(list4)

            list5 = [cutPolygon.newPoints[10], cutPolygon.newPoints[11], cutPolygon.newPoints[14],
                     cutPolygon.newPoints[13]]
            poly5 = Polygon.Polygon(list5)
            poly5.SetPolygon(list5)

            self.assertTrue(
                abs(poly1.Area() + poly2.Area() + poly3.Area() + poly4.Area() + poly5.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    # test minecraft man figure

    def test_CutPolygons_MineCraftMan(self):

        try:
            p0 = Point.Point(5.0, 1.0)
            p1 = Point.Point(7.0, 1.0)
            p2 = Point.Point(7.0, 5.0)
            p3 = Point.Point(8.0, 5.0)
            p4 = Point.Point(8.0, 1.0)
            p5 = Point.Point(10.0, 1.0)
            p6 = Point.Point(10.0, 9.0)
            p7 = Point.Point(10.2, 9.0)
            p8 = Point.Point(10.2, 5.0)
            p9 = Point.Point(12.0, 5.0)
            p10 = Point.Point(12.0, 10.8)
            p11 = Point.Point(9.0, 10.8)
            p12 = Point.Point(9.0, 13.0)
            p13 = Point.Point(6.0, 13.0)
            p14 = Point.Point(6.0, 10.8)
            p15 = Point.Point(3.0, 10.8)
            p16 = Point.Point(3.0, 5.0)
            p17 = Point.Point(4.8, 5.0)
            p18 = Point.Point(4.8, 9.0)
            p19 = Point.Point(5.0, 9.0)

            vertices = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19]
            indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
            s0 = Point.Point(4.5, 11.0)
            s1 = Point.Point(9.0, 7.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon(vertices)
            polygon.Indices = indices

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(vertices, indices, segment)

            self.assertEqual(len(cutPolygon.newPolygons), 3)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 2, 3, 4, 5, 20, 24, 15, 16, 17, 18, 19])
            self.assertListEqual(cutPolygon.newPolygons[1], [20, 6, 7, 21, 22, 9, 10, 11, 12, 13, 14, 24, 23])
            self.assertListEqual(cutPolygon.newPolygons[2], [21, 8, 22])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[2], cutPolygon.newPoints[3],
                     cutPolygon.newPoints[4], cutPolygon.newPoints[5], cutPolygon.newPoints[20],
                     cutPolygon.newPoints[24],
                     cutPolygon.newPoints[15], cutPolygon.newPoints[16], cutPolygon.newPoints[17],
                     cutPolygon.newPoints[18], cutPolygon.newPoints[19]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[20], cutPolygon.newPoints[6], cutPolygon.newPoints[7],
                     cutPolygon.newPoints[21], cutPolygon.newPoints[22],
                     cutPolygon.newPoints[9], cutPolygon.newPoints[10], cutPolygon.newPoints[11],
                     cutPolygon.newPoints[12], cutPolygon.newPoints[13],
                     cutPolygon.newPoints[14], cutPolygon.newPoints[24], cutPolygon.newPoints[23]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            list3 = [cutPolygon.newPoints[21], cutPolygon.newPoints[8], cutPolygon.newPoints[22]]
            poly3 = Polygon.Polygon(list3)
            poly3.SetPolygon(list3)

            self.assertTrue(abs(poly1.Area() + poly2.Area() + poly3.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    def test_CutPolygons_Diamond(self):

        try:
            p0 = Point.Point(7.0, 1.0)
            p1 = Point.Point(11.0, 5.0)
            p2 = Point.Point(9.7, 7.0)
            p3 = Point.Point(4.3, 7.0)
            p4 = Point.Point(3.0, 5.0)

            vertices = [p0, p1, p2, p3, p4]
            indices = [0, 1, 2, 3, 4]
            s0 = Point.Point(5.0, 5.0)
            s1 = Point.Point(12.0, 5.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon(vertices)
            polygon.Indices = indices

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(vertices, indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[5].X - 5.0) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[5].Y - 5.0) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 2)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 5, 4])
            self.assertListEqual(cutPolygon.newPolygons[1], [1, 2, 3, 4, 5])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[5], cutPolygon.newPoints[4]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[1], cutPolygon.newPoints[2], cutPolygon.newPoints[3], cutPolygon.newPoints[4],
                     cutPolygon.newPoints[5]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            self.assertTrue(abs(poly1.Area() + poly2.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()

    def test_CutPolygons_Polygon2(self):

        try:
            p0 = Point.Point(1.0, -2.0)
            p1 = Point.Point(5.0, -2.0)
            p2 = Point.Point(8.0, 1.0)
            p3 = Point.Point(10.0, 5.0)
            p4 = Point.Point(11.0, 8.0)
            p5 = Point.Point(11.0, 12.0)
            p6 = Point.Point(7.0, 14.0)
            p7 = Point.Point(5.0, 13.0)
            p8 = Point.Point(3.0, 11.0)
            p9 = Point.Point(1.0, 7.0)
            p10 = Point.Point(-1.0, 2.0)
            p11 = Point.Point(-2.0, -1.0)

            vertices = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11]
            indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
            s0 = Point.Point(0.0, 10.0)
            s1 = Point.Point(10.0, 0.0)
            segment = [s0, s1]
            intersection = Intersection()
            polygon = Polygon.Polygon(vertices)
            polygon.Indices = indices

            cutPolygon = CutPolygon(polygon, intersection)

            cutPolygon.CutPolygons(vertices, indices, segment)

            self.assertTrue(abs(cutPolygon.newPoints[12].X - 8.33333) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[12].Y - 1.66667) < 10e-5)

            self.assertTrue(abs(cutPolygon.newPoints[13].X - 1.66667) < 10e-5)
            self.assertTrue(abs(cutPolygon.newPoints[13].Y - 8.33333) < 10e-5)

            self.assertEqual(len(cutPolygon.newPolygons), 2)

            self.assertListEqual(cutPolygon.newPolygons[0], [0, 1, 2, 12, 13, 9, 10, 11])
            self.assertListEqual(cutPolygon.newPolygons[1], [12, 3, 4, 5, 6, 7, 8, 13])

            list1 = [cutPolygon.newPoints[0], cutPolygon.newPoints[1], cutPolygon.newPoints[2],
                     cutPolygon.newPoints[12], cutPolygon.newPoints[13],
                     cutPolygon.newPoints[9], cutPolygon.newPoints[10], cutPolygon.newPoints[11]]
            poly1 = Polygon.Polygon(list1)
            poly1.SetPolygon(list1)

            list2 = [cutPolygon.newPoints[12], cutPolygon.newPoints[3], cutPolygon.newPoints[4],
                     cutPolygon.newPoints[5], cutPolygon.newPoints[6],
                     cutPolygon.newPoints[7], cutPolygon.newPoints[8], cutPolygon.newPoints[13]]
            poly2 = Polygon.Polygon(list2)
            poly2.SetPolygon(list2)

            self.assertTrue(abs(poly1.Area() + poly2.Area() - polygon.Area()) < 10e-8)

            cutPolygon.CreateScriptMatLabOutput()

        except Exception as ex:
            self.fail()


