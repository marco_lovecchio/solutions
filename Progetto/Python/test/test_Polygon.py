from unittest import TestCase

import src.Polygon as Polygon
import src.Point as Point


class TestPolygon(TestCase):

    # test constructor
    def test__init__(self):
        p1 = Point.Point(1.5, 1.0)
        p2 = Point.Point(5.6, 1.5)
        p3 = Point.Point(5.5, 4.8)
        vertices = [p1, p2, p3]
        poly = Polygon.Polygon(vertices)
        poly2 = poly

        try:
            self.assertEqual(poly.Vertices[0], p1)
            self.assertEqual(poly.Vertices[1], p2)
            self.assertEqual(poly.Vertices[2], p3)
        except Exception as ex:
            self.fail()

        try:
            self.assertTrue(id(poly) is not id(poly2))
            self.assertEqual(poly2.Vertices[0], poly.Vertices[0])
            self.assertEqual(poly2.Vertices[1], poly.Vertices[1])
            self.assertEqual(poly2.Vertices[2], poly.Vertices[2])
        except Exception as ex:
            self.fail()

    def test_SetPolygon(self):
        p1 = Point.Point(1.5, 1.0)
        p2 = Point.Point(5.6, 1.5)
        p3 = Point.Point(5.5, 4.8)
        vertices = [p1, p2, p3]
        poly = Polygon.Polygon(vertices)
        poly.SetPolygon(vertices)

        try:
            self.assertEqual(len(poly.Indices), 3)
            self.assertEqual(poly.NumVertices, 3)
            self.assertEqual(poly.Indices[0], 0)
            self.assertEqual(poly.Indices[1], 1)
            self.assertEqual(poly.Indices[2], 2)
            self.assertEqual(poly.Vertices[0], p1)
            self.assertEqual(poly.Vertices[1], p2)
            self.assertEqual(poly.Vertices[2], p3)
        except Exception as ex:
            self.fail()

    def test_PolygonNumberVertices(self):
        p1 = Point.Point(1.5, 1.0)
        p2 = Point.Point(5.6, 1.5)
        p3 = Point.Point(5.5, 4.8)
        vertices = [p1, p2, p3]
        poly = Polygon.Polygon(vertices)
        poly.SetPolygon(vertices)

        try:
            self.assertEqual(poly.PolygonNumberVertices(), 3)
        except Exception as ex:
            self.fail()

    def test_GetVertex(self):
        p1 = Point.Point(1.5, 1.0)
        p2 = Point.Point(5.6, 1.5)
        p3 = Point.Point(5.5, 4.8)
        vertices = [p1, p2, p3]
        poly = Polygon.Polygon(vertices)
        poly.SetPolygon(vertices)

        try:
            self.assertEqual(poly.GetVertex(0), p1)
            self.assertEqual(poly.GetVertex(1), p2)
            self.assertEqual(poly.GetVertex(2), p3)
        except Exception as ex:
            self.fail()

    def test_Area(self):
        p0 = Point.Point(1.5, 1.0)
        p1 = Point.Point(5.6, 1.5)
        p2 = Point.Point(5.5, 4.8)
        p3 = Point.Point(4.0, 6.2)
        p4 = Point.Point(3.2, 4.2)
        p5 = Point.Point(1.0, 4.0)
        vertices = [p0, p1, p2, p3, p4, p5]
        poly = Polygon.Polygon(vertices)
        poly.SetPolygon(vertices)

        try:
            self.assertTrue(abs(poly.Area() - 15.37) < 1e-8)
        except Exception as ex:
            self.fail()
