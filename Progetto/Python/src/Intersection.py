from abc import ABC, abstractmethod
# ABC: Abstract Base Classes
from src.Point import Point
import numpy as np
import src.Enumerations as Enum

TOLP = 1e-7
TOLI = 1e-7


class IIntersection(ABC):

    @abstractmethod
    def SetFirstSegment(self, origin: Point, end: Point):
        pass

    @abstractmethod
    def SetSecondSegment(self, origin: Point, end: Point):
        pass

    @abstractmethod
    def ComputeIntersection(self) -> bool:
        pass

    @abstractmethod
    def PositionIntersectionInFirstEdge(self) -> Enum.Position:
        pass

    @abstractmethod
    def PositionIntersectionInSecondEdge(self) -> Enum.Position:
        pass

    @abstractmethod
    def TypeIntersection(self) -> Enum.Type:
        pass

    @abstractmethod
    def IntersectionPoint(self, origin: Point, end: Point) -> Point:
        pass

    @abstractmethod
    def FirstSegmentTangent(self) -> np.array(2):
        pass

    @abstractmethod
    def SecondSegmentNormal(self) -> np.array(2):
        pass


class Intersection(IIntersection):

    # this is the constructor of a class
    def __init__(self):
        self.type: Enum.Type

        self.positionIntersectionFirstEdge: Enum.Position
        self.positionIntersectionSecondEdge: Enum.Position

        self.resultParametricCoordinates = np.array([[0.0], [0.0]])
        self.originFirstSegment = np.array([[0.0], [0.0]])
        self.rightHandSide = np.array([[0.0], [0.0]])
        self.matrixTangentVector = np.array([[0.0, 0.0], [0.0, 0.0]])
        self.Origin = np.array([[0.0], [0.0]])
        self.Last = np.array([[0.0], [0.0]])
        self.Tangent1 = np.array([[0.0], [0.0]])
        self.Tangent2 = np.array([[0.0], [0.0]])
        self.Normal2 = np.array([[0.0], [0.0]])

    def SetFirstSegment(self, origin: Point, end: Point):
        self.Origin = np.array([[origin.X], [origin.Y]])
        self.Last = np.array([[end.X], [end.Y]])
        self.matrixTangentVector[0, 0] = (self.Last - self.Origin)[0]
        self.matrixTangentVector[1, 0] = (self.Last - self.Origin)[1]
        self.Tangent1 = self.matrixTangentVector[:, 0] / np.linalg.norm(self.matrixTangentVector[:, 0])
        self.originFirstSegment = self.Origin

    def SetSecondSegment(self, origin: Point, end: Point):
        self.Origin = np.array([[origin.X], [origin.Y]])
        self.Last = np.array([[end.X], [end.Y]])
        self.matrixTangentVector[0, 1] = (self.Origin - self.Last)[0]
        self.matrixTangentVector[1, 1] = (self.Origin - self.Last)[1]
        self.Tangent2 = self.matrixTangentVector[:, 1] / np.linalg.norm(self.matrixTangentVector[:, 1])
        self.Normal2[0] = - self.Tangent2[1]
        self.Normal2[1] = self.Tangent2[0]
        self.rightHandSide = self.Origin - self.originFirstSegment

    def ComputeIntersection(self) -> bool:
        parallelism: float = np.linalg.det(self.matrixTangentVector)
        self.type = Enum.Type.NoIntersection
        intersection: bool = False
        check = TOLP * TOLP * np.linalg.norm(self.matrixTangentVector[:, 0]) * np.linalg.norm(self.matrixTangentVector[:, 1])

        if parallelism * parallelism >= check:
            solverMatrix = np.array([[self.matrixTangentVector[1, 1], -self.matrixTangentVector[0, 1]],
                                     [-self.matrixTangentVector[1, 0], self.matrixTangentVector[0, 0]]])
            self.resultParametricCoordinates = np.dot(solverMatrix, self.rightHandSide)
            self.resultParametricCoordinates /= parallelism

            if self.resultParametricCoordinates[1] > -TOLI and \
                    self.resultParametricCoordinates[1] - 1.0 < TOLI:
                self.type = Enum.Type.IntersectionOnLine
                intersection = True

                if self.resultParametricCoordinates[0] > - TOLI and \
                        self.resultParametricCoordinates[0] - 1.0 < TOLI:
                    self.type = Enum.Type.IntersectionOnSegment
        else:
            parallelism2: float = abs(self.matrixTangentVector[0, 0] * self.rightHandSide[1] -
                                      self.rightHandSide[0] * self.matrixTangentVector[1, 0])
            squaredNormFirstEdge: float = np.linalg.norm(self.matrixTangentVector[:, 0])
            check2: float = TOLP * TOLP * squaredNormFirstEdge * np.linalg.norm(self.rightHandSide)

            if parallelism2 * parallelism2 <= check2:
                tempNorm: float = 1.0/squaredNormFirstEdge

                self.resultParametricCoordinates[0] = \
                    np.dot(self.matrixTangentVector[:, 0], self.rightHandSide) * tempNorm

                self.resultParametricCoordinates[1] = \
                    self.resultParametricCoordinates[0] - \
                    np.dot(self.matrixTangentVector[:, 0], self.matrixTangentVector[:, 1]) * tempNorm

                intersection = True
                self.type = Enum.Type.IntersectionParallelOnLine

                if self.resultParametricCoordinates[1] < self.resultParametricCoordinates[0]:
                    tmp: float = self.resultParametricCoordinates[0]
                    self.resultParametricCoordinates[0] = self.resultParametricCoordinates[1]
                    self.resultParametricCoordinates[1] = tmp

                if (self.resultParametricCoordinates[0] > - TOLI and
                    self.resultParametricCoordinates[0]-1.0 < TOLI) or \
                        (self.resultParametricCoordinates[1] > - TOLI and
                         self.resultParametricCoordinates[1]-1.0 < TOLI):
                    self.type = Enum.Type.IntersectionParallelOnSegment
                else:
                    if self.resultParametricCoordinates[0] < TOLI and self.resultParametricCoordinates[1] - 1.0 > - TOLI:
                        self.type = Enum.Type.IntersectionParallelOnSegment

        if self.resultParametricCoordinates[0] < -TOLI or \
                self.resultParametricCoordinates[0] > 1.0 + TOLI:
            self.positionIntersectionFirstEdge = Enum.Position.Outer
        elif - TOLI < self.resultParametricCoordinates[0] < TOLI:
            self.resultParametricCoordinates[0] = 0.0
            self.positionIntersectionFirstEdge = Enum.Position.Begin
        elif 1.0 - TOLI < self.resultParametricCoordinates[0] < 1.0 + TOLI:
            self.resultParametricCoordinates[0] = 1.0
            self.positionIntersectionFirstEdge = Enum.Position.End
        else:
            self.positionIntersectionFirstEdge = Enum.Position.Inner

        if self.resultParametricCoordinates[1] < -TOLI or \
                self.resultParametricCoordinates[1] > 1.0 + TOLI:
            self.positionIntersectionSecondEdge = Enum.Position.Outer
        elif self.resultParametricCoordinates[1] > -TOLI and \
                self.resultParametricCoordinates[1] < TOLI:
            self.resultParametricCoordinates[1] = 0.0
            self.positionIntersectionSecondEdge = Enum.Position.Begin
        elif 1.0 - TOLI < self.resultParametricCoordinates[1] <= 1.0 + TOLI:
            self.resultParametricCoordinates[1] = 1.0
            self.positionIntersectionSecondEdge = Enum.Position.End
        else:
            self.positionIntersectionSecondEdge = Enum.Position.Inner

        return intersection

    def PositionIntersectionInFirstEdge(self) -> Enum.Position:
        return self.positionIntersectionFirstEdge

    def PositionIntersectionInSecondEdge(self) -> Enum.Position:
        return self.positionIntersectionSecondEdge

    def TypeIntersection(self) -> Enum.Type:
        return self.type

    def IntersectionPoint(self, origin: Point, end: Point) -> Point:
        x: float = (1 - self.resultParametricCoordinates[0]) * origin.X + self.resultParametricCoordinates[0] * end.X
        y: float = (1 - self.resultParametricCoordinates[0]) * origin.Y + self.resultParametricCoordinates[0] * end.Y
        return Point(x, y)

    def FirstSegmentTangent(self) -> np.array(2):
        return self.Tangent1

    def SecondSegmentNormal(self) -> np.array(2):
        return self.Normal2
