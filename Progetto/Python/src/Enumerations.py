from enum import Enum


class Type(Enum):
    NoIntersection = 0  # Nessuna intersezione, paralleli
    IntersectionOnLine = 1  # Interseca il prolungamento del segmento
    IntersectionOnSegment = 2  # Interseca il  segmento
    IntersectionParallelOnLine = 3  # Giace sul prolungamento  del segmento
    IntersectionParallelOnSegment = 4  #Giace sul segmento


class Position(Enum):
    Begin = 0  # Inizio
    Inner = 1
    End = 2  # Fine
    Outer = 3

