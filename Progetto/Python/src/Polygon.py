from src.Point import Point
from abc import ABC, abstractmethod


class IPolygon(ABC):
    @abstractmethod
    def SetPolygon(self, vertices: ()):
        pass

    @abstractmethod
    def PolygonNumberVertices(self) -> int:
        pass

    @abstractmethod
    def GetVertex(self, index: int) -> Point:
        pass

    @abstractmethod
    def Area(self) -> float:
        pass


class Polygon(IPolygon):

    def __init__(self, vertices: list[Point]):

        self.Vertices = vertices
        self.Indices = []
        self.NumVertices: int = len(self.Vertices)

    def SetPolygon(self, vertices: []):

        for i in range(0, len(self.Vertices)):
            self.Indices.append(i)

    def PolygonNumberVertices(self) -> int:
        return self.NumVertices

    def GetVertex(self, index: int) -> Point:
        return self.Vertices[index]

    def Area(self) -> float:

        area: float = 0

        for i in range(0, self.PolygonNumberVertices()):

            area += 0.5 * (self.GetVertex(i).X * self.GetVertex((i+1) % self.PolygonNumberVertices()).Y -
                           self.GetVertex((i+1) % self.PolygonNumberVertices()).X * self.GetVertex(i).Y)
        return area
