class Point:
    def __init__(self, x: float, y: float):
        self.X = x
        self.Y = y

    def ComputeNorm2(self):
        return pow(self.X*self.X + self.Y*self.Y, 0.5)

    # operatore somma
    def __add__(self, other):
        return Point(self.X + other.X, self.Y + other.Y)

    # operatore sottrazione
    def __sub__(self, other):
        return Point(self.X - other.X, self.Y - other.Y)

    # operatore minore
    def __lt__(self, other) -> bool:

        if self.X < other.X:
            return True
        else:
            if self.X == other.X:
                if self.Y < other.Y:
                    return True
                return False

            return False

    # operatore di uguaglianza
    def __eq__(self, other) -> bool:

        if other.X == self.X and other.Y == self.Y:
            return True
        else:
            return False
