import os.path

import numpy as np
import src.Polygon as Polygon
import src.Intersection as Intersection
from src.Enumerations import Type, Position
from src.Point import Point


class CutPolygon:

    def __init__(self, polygon: Polygon.IPolygon, intersection: Intersection.IIntersection):
        self.__Polygon = polygon
        self.__Intersection = intersection
        self.newPolygons = [[]]
        self.newPoints = []
        self.__Indices = []
        self.__Segment = []

    def CutPolygons(self, vertices: [Point], indices: [int], segment: [Point]):
        self.__Polygon.SetPolygon(vertices)
        self.__Intersection.SetFirstSegment(segment[0], segment[1])
        self.__Segment = segment

        numVertices = self.__Polygon.PolygonNumberVertices()

        for i in range(0, numVertices):
            self.newPoints.append(self.__Polygon.GetVertex(i))
            self.__Indices.append(indices[i])

        intersectionPoint = []
        orderedPoint = []
        toControlPoint = []
        intersectionIndex = []
        orderedIndex = []
        toControlIndex = []
        inOrOut = []
        rightOrLeft = []
        parallel = []
        parallelismo: int = 0
        newIndex: int = numVertices
        counter: int = 0
        added: int = 0

        for i in range(0, numVertices):
            self.__Intersection.SetSecondSegment(self.__Polygon.GetVertex(i), self.__Polygon.GetVertex((i+1) % numVertices))
            if self.__Intersection.ComputeIntersection():
                if (self.__Intersection.TypeIntersection() is Type.IntersectionOnLine or
                    self.__Intersection.TypeIntersection() is Type.IntersectionOnSegment) and \
                        self.__Intersection.PositionIntersectionInSecondEdge() is not Position.Begin:
                    counter += 1
                    intersectionPoint.append(self.__Intersection.IntersectionPoint(self.__Segment[0], self.__Segment[1]))
                    orderedPoint.append(self.__Polygon.GetVertex(i))
                    orderedIndex.append(self.__Indices[i])
                    if counter > 1:
                        parallel.append(False)
                        if intersectionPoint[counter - 2] < intersectionPoint[counter - 1] and \
                                np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) > 0:
                            if intersectionPoint[counter - 2] < self.__Segment[0] < intersectionPoint[counter - 1] < self.__Segment[1] \
                                    and added < 2:
                                self.newPoints.append(self.__Segment[0])
                                orderedPoint.append(self.__Segment[0])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                            elif intersectionPoint[counter - 2] < self.__Segment[0] and \
                                    self.__Segment[1] < intersectionPoint[counter - 1] and added < 2:
                                self.newPoints.append(self.__Segment[0])
                                orderedPoint.append(self.__Segment[0])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                                self.newPoints.append(self.__Segment[1])
                                orderedPoint.append(self.__Segment[1])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                            elif self.__Segment[0] < intersectionPoint[counter - 2] < self.__Segment[1] < intersectionPoint[counter - 1] \
                                    and added < 2:
                                self.newPoints.append(self.__Segment[1])
                                orderedPoint.append(self.__Segment[1])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                            inOrOut.append(True)
                            rightOrLeft.append(True)
                        elif intersectionPoint[counter - 1] < intersectionPoint[counter - 2] and \
                                np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) < 0:
                            if intersectionPoint[counter - 1] < self.__Segment[0] < intersectionPoint[counter - 2] < self.__Segment[1] \
                                     and added < 2:
                                self.newPoints.append(self.__Segment[0])
                                orderedPoint.append(self.__Segment[0])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                            elif intersectionPoint[counter - 1] < self.__Segment[0] and \
                                    self.__Segment[1] < intersectionPoint[counter - 2] and added < 2:
                                self.newPoints.append(self.__Segment[1])
                                orderedPoint.append(self.__Segment[1])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                                self.newPoints.append(self.__Segment[0])
                                orderedPoint.append(self.__Segment[0])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                            elif self.__Segment[0] < intersectionPoint[counter - 1] < self.__Segment[1] < intersectionPoint[counter - 2] \
                                    and added < 2:
                                self.newPoints.append(self.__Segment[1])
                                orderedPoint.append(self.__Segment[1])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                            inOrOut.append(True)
                            rightOrLeft.append(False)
                        elif intersectionPoint[counter - 2] < intersectionPoint[counter - 1] and \
                                np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) < 0:
                            inOrOut.append(False)
                            rightOrLeft.append(True)
                        elif intersectionPoint[counter - 1] < intersectionPoint[counter - 2] and \
                                np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) > 0:
                            inOrOut.append(False)
                            rightOrLeft.append(False)
                    if self.__Intersection.PositionIntersectionInSecondEdge() is not Position.End:
                        self.newPoints.append(self.__Intersection.IntersectionPoint(self.__Segment[0], self.__Segment[1]))
                        orderedPoint.append(self.__Intersection.IntersectionPoint(self.__Segment[0], self.__Segment[1]))
                        orderedIndex.append(newIndex)
                        intersectionIndex.append(newIndex)
                        newIndex += 1
                    else:
                        intersectionIndex.append(self.__Indices[(i+1) % numVertices])
                        toControlPoint.append(self.__Polygon.GetVertex((i+1) % numVertices))
                        toControlIndex.append(self.__Indices[(i+1) % numVertices])
                elif self.__Intersection.TypeIntersection() is Type.IntersectionParallelOnSegment or \
                        self.__Intersection.TypeIntersection() is Type.IntersectionParallelOnLine:
                    parallelismo += 1
                    orderedPoint.append(self.__Polygon.GetVertex(i))
                    orderedIndex.append(self.__Indices[i])
                    if self.__Polygon.GetVertex(i) < self.__Polygon.GetVertex(i+1):
                        if self.__Polygon.GetVertex(i) < self.__Segment[0] < self.__Polygon.GetVertex(i+1):
                            self.newPoints.append(self.__Segment[0])
                            orderedPoint.append(self.__Segment[0])
                            added += 1
                            orderedIndex.append(newIndex)
                            newIndex += 1
                            if self.__Segment[1] < self.__Polygon.GetVertex(i+1):
                                self.newPoints.append(self.__Segment[1])
                                orderedPoint.append(self.__Segment[1])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                        elif self.__Segment[0] < self.__Polygon.GetVertex(i) < self.__Segment[1] < self.__Polygon.GetVertex(i + 1):
                            self.newPoints.append(self.__Segment[1])
                            orderedPoint.append(self.__Segment[1])
                            added += 1
                            orderedIndex.append(newIndex)
                            newIndex += 1
                    else:
                        if self.__Polygon.GetVertex(i + 1) < self.__Segment[1] < self.__Polygon.GetVertex(i):
                            self.newPoints.append(self.__Segment[1])
                            orderedPoint.append(self.__Segment[1])
                            added += 1
                            orderedIndex.append(newIndex)
                            newIndex += 1
                            if self.__Polygon.GetVertex(i+1) < self.__Segment[0]:
                                self.newPoints.append(self.__Segment[0])
                                orderedPoint.append(self.__Segment[0])
                                added += 1
                                orderedIndex.append(newIndex)
                                newIndex += 1
                        elif self.__Polygon.GetVertex(i + 1) < self.__Segment[0] < self.__Polygon.GetVertex(i) < self.__Segment[1]:
                            self.newPoints.append(self.__Segment[0])
                            orderedPoint.append(self.__Segment[0])
                            added += 1
                            orderedIndex.append(newIndex)
                            newIndex += 1
                else:
                    orderedPoint.append(self.__Polygon.GetVertex(i))
                    orderedIndex.append(self.__Indices[i])
                    if parallelismo > 0:
                        counter += 1
                        intersectionPoint.append(self.__Polygon.GetVertex(i))
                        intersectionIndex.append(self.__Indices[i])
                        if counter > 1:
                            parallel.append(True)
                            if intersectionPoint[counter - 2] < intersectionPoint[counter - 1] and \
                                    np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) > 0:
                                inOrOut.append(True)
                                rightOrLeft.append(True)
                            elif intersectionPoint[counter - 1] < intersectionPoint[counter - 2] and \
                                    np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) < 0:
                                inOrOut.append(True)
                                rightOrLeft.append(False)
                            elif intersectionPoint[counter - 2] < intersectionPoint[counter - 1] and \
                                    np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) < 0:
                                inOrOut.append(False)
                                rightOrLeft.append(True)
                            elif intersectionPoint[counter - 1] < intersectionPoint[counter - 2] and \
                                    np.dot(self.__Intersection.FirstSegmentTangent(), self.__Intersection.SecondSegmentNormal()) > 0:
                                inOrOut.append(False)
                                rightOrLeft.append(False)
            else:
                orderedPoint.append(self.__Polygon.GetVertex(i))
                orderedIndex.append(self.__Indices[i])

        # ridimensionamenti automatici di Python
        # intersectionIndex = intersectionIndex[counter]
        # intersectionPoint = intersectionPoint[counter]
        # rightOrLeft = rightOrLeft[counter - 1]
        # inOrOut = inOrOut[counter - 1]

        numPolygons: int = 1
        if parallelismo > 0:
            numPolygons = counter - parallelismo
            for i in range (0,numPolygons-1):
                self.newPolygons.append([])
        else:
            for i in range(0, counter - 1):
                if inOrOut[i]:
                    numPolygons += 1
                    self.newPolygons.append([])

        toInsert = 0
        trik = 0
        start = 0
        end = len(orderedPoint)
        temp = 0
        numControlli = len(toControlPoint)
        if parallelismo == 1:
            for j in range(start, end):
                self.newPolygons[0].append(orderedIndex[j])
        else:
            for i in range(0, numPolygons):
                if i == 0:
                    if numPolygons > 2 and numControlli > 0 and numPolygons is counter:
                        j = start
                        while j < end:
                            if orderedIndex[j] is not intersectionIndex[0]:
                                self.newPolygons[i].append(orderedIndex[j])
                            else:
                                start = j
                                self.newPolygons[i].append(orderedIndex[j])
                                self.newPolygons[i].append(intersectionIndex[counter - 1])
                                while orderedIndex[j] is not intersectionIndex[counter - 1]:
                                    j += 1
                                temp = j
                            j += 1
                    else:
                        j = start
                        while j < end:
                            if orderedIndex[j] is not intersectionIndex[0]:
                                self.newPolygons[i].append(orderedIndex[j])
                            else:
                                start = j
                                self.newPolygons[i].append(intersectionIndex[0])
                                for k in range(0, counter - 1):
                                    if inOrOut[k] is False and rightOrLeft[k]:
                                        k = counter - 1
                                        if numPolygons > 2 and numControlli > 0:
                                            for y in range(0, numControlli):
                                                if intersectionPoint[k] > toControlPoint[y] > intersectionPoint[0] or \
                                                        intersectionPoint[k] < toControlPoint[y] < intersectionPoint[0]:
                                                    self.newPolygons[i].append(toControlIndex[y])
                                        self.newPolygons[i].append(intersectionIndex[k])
                                        while orderedIndex[j] is not intersectionIndex[k]:
                                            j += 1
                                        temp = j
                                        break
                                    elif inOrOut[k] is False and rightOrLeft[k] is False:
                                        while orderedIndex[j] is not intersectionIndex[k+1]:
                                            j += 1
                                            self.newPolygons[i].append(orderedIndex[j])
                                    elif inOrOut[k] is True:
                                        while orderedIndex[j] is not intersectionIndex[k+1]:
                                            if orderedPoint[j] is self.__Segment[0] or orderedPoint[j] is self.__Segment[1]:
                                                self.newPolygons[i].append(orderedIndex[j])
                                            j += 1
                                        if numPolygons > 2 and numControlli > 0:
                                            for y in range(0, numControlli):
                                                if intersectionPoint[k] < toControlPoint[y] < intersectionPoint[k + 1] or \
                                                        intersectionPoint[k+1] < toControlPoint[y] < intersectionPoint[k]:
                                                    self.newPolygons[i].append(toControlIndex[y])
                                        self.newPolygons[i].append(orderedIndex[j])
                                        temp = j
                            j += 1
                    end = temp
                else:
                    j = start
                    while j < end:
                        k = i - 1 + trik
                        while k < counter - 1:
                            if parallel[k] is True:
                                trik += 1
                            if (k is i - 1 + trik and inOrOut[k] is True) or \
                                    (inOrOut[k] is True and inOrOut[k-1] is False and rightOrLeft[k-1] is False):
                                while orderedIndex[j] is not intersectionIndex[k+1]:
                                    if orderedPoint[j] is not self.__Segment[0] and orderedPoint[j] is not self.__Segment[1]:
                                        self.newPolygons[i].append(orderedIndex[j])
                                    else:
                                        toInsert += 1
                                    j += 1
                                self.newPolygons[i].append(orderedIndex[j])
                                if toInsert > 0:
                                    if toInsert == 2:
                                        self.newPolygons[i].append(orderedIndex[j-1])
                                        self.newPolygons[i].append(orderedIndex[j-2])
                                    else:
                                        self.newPolygons[i].append(orderedIndex[j-1])
                                if numPolygons > 2 and numControlli > 0:
                                    for y in range(0, numControlli):
                                        if ((intersectionPoint[k] < toControlPoint[y] < intersectionPoint[k+1] and intersectionPoint[k] < intersectionPoint[k+1]) \
                                                or (intersectionPoint[k+1] < toControlPoint[y] < intersectionPoint[k] and intersectionPoint[k+1] < intersectionPoint[k])):
                                            self.newPolygons[i].append(toControlIndex[y])
                                    if (self.newPolygons[i][len(self.newPolygons[i])-2] == self.newPolygons[i][len(self.newPolygons[i])-1] or
                                            self.newPolygons[i][len(self.newPolygons[i])-1] == self.newPolygons[i][0]):
                                        self.newPolygons[i].pop(len(self.newPolygons[i])-1)
                                toInsert = 0
                                if parallelismo == 0 and k + 1 < counter - 1 and k-1 >= 0 and \
                                        inOrOut[k-1] is False and rightOrLeft[k-1] is True and inOrOut[k] is True and \
                                        rightOrLeft[k] is True and inOrOut[k+1] is True and rightOrLeft[k+1] is False:
                                    while orderedIndex[j] is not intersectionIndex[k+2]:
                                        j += 1
                                    trik += 1
                                start = j
                                j = end
                                break
                            elif inOrOut[k] is False and rightOrLeft[k] is True:
                                if k is i - 1:
                                    self.newPolygons[i].append(intersectionIndex[k])
                                while orderedIndex[j] is not intersectionIndex[k+1]:
                                    j += 1
                                    self.newPolygons[i].append(orderedIndex[j])
                            elif inOrOut[k] is False and rightOrLeft[k] is False:
                                while orderedIndex[j] is not intersectionIndex[k+1]:
                                    j += 1
                            elif inOrOut[k] is True and rightOrLeft[k] is True:
                                start = j
                                while orderedIndex[j] is not intersectionIndex[k+1]:
                                    if orderedPoint[j] is self.__Segment[0] or orderedPoint[j] is self.__Segment[1]:
                                        self.newPolygons[i].append(orderedIndex[j])
                                    j += 1
                                self.newPolygons[i].append(orderedIndex[j])
                            elif inOrOut[k] is True and rightOrLeft[k] is False:
                                while orderedIndex[j] is not intersectionIndex[k + 1]:
                                    j += 1
                                    if orderedPoint[j] is self.__Segment[0] or orderedPoint[j] is self.__Segment[1]:
                                        toInsert += 1
                                        j += 1
                                    self.newPolygons[i].append(orderedIndex[j])
                                if toInsert > 0:
                                    if toInsert == 2:
                                        self.newPolygons[i].append(orderedIndex[j-1])
                                        self.newPolygons[i].append(orderedIndex[j-2])
                                    else:
                                        self.newPolygons[i].append(orderedIndex[j-1])
                                toInsert = 0
                                j = end
                                k = counter - 1
                            k += 1
                        j += j


    def CreateScriptMatLabOutput(self):

        # if the file already exists, open the file to append:
        if os.path.isfile('scriptProject.m'):

            f = open("scriptProject.m", "a")

            f.write("points = [\n")

            # scrittura lista punti

            for i in range(0, len(self.newPoints)):
                f.write(str(self.newPoints[i].X) + ", " + str(self.newPoints[i].Y) + ";\n")

            f.write("];\n")

            # assemblaggio poligoni

            for i in range(0, len(self.newPolygons)):

                f.write("polygon" + str(i) + " = polyshape([ ")

                for j in range(0, len(self.newPolygons[i])):
                    f.write("points(" + str(int(self.newPolygons[i][j]) + 1) + ", 1) ")

                f.write("], [ ")

                for j in range(0, len(self.newPolygons[i])):
                    f.write("points(" + str(self.newPolygons[i][j] + 1) + ", 2) ")

                f.write("]);\n")

            # assemblaggio segmento
            f.write("segment = [" + str(self.__Segment[0].X) + ", " + str(self.__Segment[0].Y) + ";\n")
            f.write(str(self.__Segment[1].X) + ", " + str(self.__Segment[1].Y) + "];\n")
            f.write("figure;\n")

            for i in range(0, len(self.newPolygons)):
                f.write("plot(polygon" + str(i) + ");\n")
                f.write("hold on;\n")

            f.write("plot(segment(:, 1), segment(:, 2), 'r--');\n")
            f.write("hold on;\n")
            f.write("plot(points(:, 1),points(:, 2), 'ko');\n")
            f.write("hold on;\n")
            f.write("plot(segment(:, 1), segment(:, 2), 'r*');")

            f.close()

        # if the file does not exists, create a new file with the name below:
        else:

            f = open("scriptProject.m", "x")

            f.write("points = [\n")

            # scrittura lista punti

            for i in range(0, len(self.newPoints)):

                f.write(str(self.newPoints[i].X) + ", " + str(self.newPoints[i].Y) + ";\n")

            f.write("];\n")

            # assemblaggio poligoni

            for i in range(0, len(self.newPolygons)):

                f.write("polygon" + str(i) + " = polyshape([ ")

                for j in range(0, len(self.newPolygons[i])):

                    f.write("points(")
                    f.write(str(int(self.newPolygons[i][j] + 1)))
                    f.write(", 1) ")

                f.write("], [ ")

                for j in range(0, len(self.newPolygons[i])):

                    f.write("points(")
                    f.write(str(self.newPolygons[i][j] + 1))
                    f.write(", 2) ")

                f.write("]);\n")

            # assemblaggio segmento
            f.write("segment = [" + str(self.__Segment[0].X) + ", " + str(self.__Segment[0].Y) + ";\n")
            f.write(str(self.__Segment[1].X) + ", " + str(self.__Segment[1].Y) + "];\n")
            f.write("figure;\n")

            for i in range(0, len(self.newPolygons)):

                f.write("plot(polygon" + str(i) + ");\n")
                f.write("hold on;\n")

            f.write("plot(segment(:, 1), segment(:, 2), 'r--');\n")
            f.write("hold on;\n")
            f.write("plot(points(:, 1),points(:, 2), 'ko');\n")
            f.write("hold on;\n")
            f.write("plot(segment(:, 1), segment(:, 2), 'r*');")

            f.close()
