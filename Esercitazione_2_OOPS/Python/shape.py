import math as m

class Point:
    def __init__(self, x: float, y: float):
        self.ax = x
        self.ay = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._c = center
        self._sa = a
        self._sb = b

    def area(self) -> float:
        return self._sa*self._sb*m.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        Ellipse.__init__(self,center, radius, radius)



class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._a = pow(pow((self._p1.ax - self._p2.ax), 2) + pow((self._p1.ay - self._p2.ay), 2), 1 / 2)
        self._b = pow(pow((self._p2.ax - self._p3.ax), 2) + pow((self._p2.ay - self._p3.ay), 2), 1 / 2)
        self._c = pow(pow((self._p1.ax - self._p3.ax), 2) + pow((self._p1.ay - self._p3.ay), 2), 1 / 2)

    def area(self) -> float:
        p = (self._a + self._b + self._c) / 2
        return pow(p*(p - self._a)*(p - self._b) * (p - self._c), 1 / 2)



class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        Triangle.__init__(self, p1, p1, p1)
        self._a = edge
        self._b = edge
        self._c = edge



class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4
        self._a = pow(pow((self._p1.ax - self._p2.ax), 2) + pow((self._p1.ay - self._p2.ay), 2), 1 / 2)
        self._b = pow(pow((self._p2.ax - self._p3.ax), 2) + pow((self._p2.ay - self._p3.ay), 2), 1 / 2)
        self._c = pow(pow((self._p3.ax - self._p4.ax), 2) + pow((self._p3.ay - self._p4.ay), 2), 1 / 2)
        self._d = pow(pow((self._p1.ax - self._p4.ax), 2) + pow((self._p1.ay - self._p4.ay), 2), 1 / 2)
        self._e = pow(pow((self._p2.ax - self._p4.ax), 2) + pow((self._p2.ay - self._p4.ay), 2), 1 / 2)

    def area(self) -> float:
        p1 = (self._a + self._d + self._e) / 2
        p2 = (self._b + self._c + self._e) / 2
        return pow(p1 * (p1 - self._a) * (p1 - self._d) * (p1 - self._e), 1 / 2) + pow(p2 * (p2 - self._b) * (p2 - self._c) * (p2 - self._e), 1 / 2)



class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        Quadrilateral.__init__(self, p1,p2,p4,p4)
        self._c = self._a
        self._b = self._d


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        Parallelogram.__init__(self, p1, p1, p1)
        self._a = base
        self._b = height
        self._c = self._a
        self._d = self._b
        self._e = pow(pow(self._a,2)+pow(self._b,2),1/2)


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        Rectangle.__init__(self,p1, edge, edge)
        self._a = edge
        self._e = edge*pow(2,1/2)
