#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <math.h>

using namespace std;

namespace ShapeLibrary {

  class Point
  {
   public:
      double ax, ay;
      Point(const double& x,
           const double& y);
      Point(const Point& point) { ax = point.ax; ay = point.ay; }
  };

  class IPolygon
  {
   public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
   protected:
      double xc, yc;
      int saxa, saxb;

   public:
      Ellipse(const Point& center,
             const int& a,
             const int& b);

   double Area() const;
  };

  class Circle : public Ellipse
  {
   public:
     Circle(const Point& center,
            const int& radius):
         Ellipse (center, radius, radius)
     {
         saxa = radius;
         saxb = radius;
     }
   };


   class Triangle : public IPolygon
   {
    protected:
       double x1, y1, x2, y2, x3, y3;
       double a, b, c;

   public:
       Triangle(const Point& p1,
                const Point& p2,
                const Point& p3);

       double Area() const;
   };

   class TriangleEquilateral : public Triangle
   {
   public:
       TriangleEquilateral(const Point& p1,
                           const int& edge):
           Triangle(p1, p1, p1)
       {
           a = edge;
           b = edge;
           c = edge;
       };
   };

   class Quadrilateral : public IPolygon
   {
   protected:
       double x1, y1, x2, y2, x3, y3, x4, y4;
       double a, b, c, d, e;

   public:
       Quadrilateral(const Point& p1,
                     const Point& p2,
                     const Point& p3,
                     const Point& p4);

       double Area() const;
   };

   class Parallelogram : public Quadrilateral
   {
    public:
       Parallelogram(const Point& p1,
                     const Point& p2,
                     const Point& p4):
           Quadrilateral(p1, p2, p4, p4)
       {
           c = a;
           b = d;
       };
   };

   class Rectangle : public Parallelogram
   {
    public:
       Rectangle(const Point& p1,
                 const int& base,
                 const int& height):
           Parallelogram(p1, p1, p1)
       {
           a = base;
           d = height;
           b = d;
           c = a;
           e = sqrt(pow(a,2)+pow(b,2));
       }
   };

   class Square: public Rectangle
   {
    public:
       Square(const Point& p1,
              const int& edge):
           Rectangle(p1, edge, edge)
       {
           a = edge;
           e = edge*sqrt(2);
       };
   };

}

#endif // SHAPE_H
