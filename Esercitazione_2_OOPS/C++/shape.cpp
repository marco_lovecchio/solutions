#include "shape.h"
#include <math.h>

namespace ShapeLibrary
{

Point::Point(const double& x, const double& y)
{
    ax = x;
    ay = y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    xc = center.ax;
    yc = center.ay;
    saxa = a;
    saxb = b;
}

double Ellipse::Area() const
{
    return M_PI*saxa*saxb;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    x1 = p1.ax;
    y1 = p1.ay;
    x2 = p2.ax;
    y2 = p2.ay;
    x3 = p3.ax;
    y3 = p3.ay;
    a = sqrt(pow((x1-x2), 2) + pow((y1-y2), 2));
    b = sqrt(pow((x2-x3), 2) + pow((y2-y3), 2));
    c = sqrt(pow((x1-x3), 2) + pow((y1-y3), 2));
}

double Triangle::Area() const
{
    double p = (a+b+c)/2;
    return sqrt(p*(p-a)*(p-b)*(p-c));
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    x1 = p1.ax;
    y1 = p1.ay;
    x2 = p2.ax;
    y2 = p2.ay;
    x3 = p3.ax;
    y3 = p3.ay;
    x4 = p4.ax;
    y4 = p4.ay;
    a = sqrt(pow((x1-x2), 2) + pow((y1-y2), 2));
    b = sqrt(pow((x2-x3), 2) + pow((y2-y3), 2));
    c = sqrt(pow((x3-x4), 2) + pow((y3-y4), 2));
    d = sqrt(pow((x4-x1), 2) + pow((y4-y1), 2));
    e = sqrt(pow((x2-x4), 2) + pow((y2-y4), 2));
}

double Quadrilateral::Area() const
{
    double p1 = (a+d+e)/2;
    double p2 = (b+c+e)/2;
    return sqrt(p1*(p1-a)*(p1-d)*(p1-e)) + sqrt(p2*(p2-b)*(p2-c)*(p2-e));
}

}
