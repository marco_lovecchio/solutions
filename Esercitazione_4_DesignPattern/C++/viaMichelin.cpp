# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    _numberBus = 0;
    _buses.clear();

    ifstream file;
    file.open(_busFilePath.c_str());

    if (file.fail())
      throw runtime_error("Something goes wrong");

    try
    {
        string line;
        getline(file, line); // Prima riga di testo da scartare
        getline(file, line);

        istringstream convertN;
        convertN.str(line);
        convertN >> _numberBus;

        getline(file, line); //stringa di intestazione da scartare
        getline(file, line); //scarta

        _buses.resize(_numberBus);
        for(int b = 0; b < _numberBus; b++ )
        {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> _buses[b].Id >> _buses[b].FuelCost;
        }
        file.close();
    }
    catch (exception)
    {
        _numberBus = 0;
        _buses.clear();

        throw runtime_error("Something goes wrong");
    }
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus > _numberBus)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
}

void MapData::Reset()
{
    _numberBusStops = 0;
    _numberStreets = 0;
    _numberRoutes = 0;
    _busStops.clear();
    _streets.clear();
    _routes.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _routeStreets.clear();
}

void MapData::Load()
{
    Reset();

    ifstream file;
    file.open(_mapFilePath.c_str());

    if (file.fail())
        throw runtime_error("Something goes wrong");

    try
    {
        string line;

        getline(file, line); //intestazione da ignorare
        getline(file, line);
        istringstream convertBusStops; //funzione di input per le stringhe
        convertBusStops.str(line);
        convertBusStops >> _numberBusStops; //assegnazione numero fermate

        getline(file, line); //intestazione da ignorare

        _busStops.resize(_numberBusStops); //ridimensionamento vettore fermate

        for(int s = 0; s < _numberBusStops; s++) //carico le fermate
        {
           getline(file, line);
           istringstream converter;
           converter.str(line);
           converter >> _busStops[s].Id >> _busStops[s].Name >> _busStops[s].Latitude >> _busStops[s].Longitude;
        }
        getline(file, line); //intestazione da ignorare

        getline(file, line);
        istringstream convertStreets;
        convertStreets.str(line);
        convertStreets >> _numberStreets; //assegnazione numero strade

        getline(file, line); //intestazione da ignorare

        _streets.resize(_numberStreets);
        _streetsFrom.resize(_numberStreets);
        _streetsTo.resize(_numberStreets); //ridimensionamento vettori strade

        for(int s = 0; s < _numberStreets; s++)
        {
           getline(file, line);
           istringstream converter;
           converter.str(line);
           converter >> _streets[s].Id >> _streetsFrom[s] >> _streetsTo[s] >> _streets[s].TravelTime; //carico strade
        }

        getline(file, line); //intestazione da ignorare
        getline(file, line);
        istringstream convertRoutes;
        convertRoutes.str(line);
        convertRoutes >> _numberRoutes; //setto il numero dei percorsi

        getline(file, line); //intestazione da ignorare
        _routes.resize(_numberRoutes); //ridimensionamento vettore percorsi
        _routeStreets.resize(_numberRoutes); //ridimensiono il numero di strade che costituitscono il percorso

        for(int r = 0; r < _numberRoutes; r++) //carico i percorsi
        {
            getline(file, line);
            istringstream converter;
            converter.str(line);
            converter >> _routes[r].Id >> _routes[r].NumberStreets;
            _routeStreets[r].resize(_routes[r].NumberStreets); //ridimensionamento in base al percorso corrente

            for(int s = 0; s <_routes[r].NumberStreets; s++)
            {
                converter >> _routeStreets[r][s];
            }
        }

        file.close();
    }
    catch (exception)
    {
        Reset();
        throw runtime_error("Something goes wrong");
    }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const //restituisce la strada di un dato percorso
{
    if (idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    const Route& route = _routes[idRoute - 1];

    if (streetPosition >= route.NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if(idRoute > _numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if(idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if (idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idFrom = _streetsFrom[idStreet - 1];

    return _busStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if (idStreet > _numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idTo = _streetsTo[idStreet - 1];

    return _busStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if (idBusStop > _numberBusStops)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;

    for (int s = 0; s < route.NumberStreets; s++)
          travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;

    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    const Bus& bus = _busStation.GetBus(idBus);

    double totalCost = 0;
    for(int s = 0; s < route.NumberStreets; s++)
    {
        totalCost += _mapData.GetRouteStreet(idRoute, s).TravelTime * BusAverageSpeed / 3600.0 * bus.FuelCost;
    }
    return totalCost;
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);
    int s = 0;
    ostringstream routeView; //funzione di output di stringhe
    //ostringstream preferibile per operazioni su flussi di stringhe (leggibilità del codice)
    //In alternativa creo string routeView = "" e tramite += incremento tramite ciclo

    routeView<< to_string(route.Id)<< ": ";

    for(; s < route.NumberStreets - 1; s++) // il ciclo si chiude alla terzultima strada del percorso
    {
        int idStreet = _mapData.GetRouteStreet(route.Id,s).Id;
        string from = _mapData.GetStreetFrom(idStreet).Name;
        routeView << from << " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(route.Id, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name; //prelevo la fermata di destinazione sapendo in input idStreet di provenienza
    routeView << from<< " -> "<< to;

    return routeView.str(); //str permette la restituzione in formato stringa di routeView
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    //fermata di partenza
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    //fermata di arrivo
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    //ViewStreet resituisce già in formato stringa
    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}
