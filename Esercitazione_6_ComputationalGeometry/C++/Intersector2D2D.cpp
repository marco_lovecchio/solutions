#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection =  1.0E-7;
    intersectionType = TypeIntersection::NoIntersection;
}

Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;
    rightHandSide[0] = planeTranslation;
    return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide[1] = planeTranslation;
    return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    intersectionType = NoIntersection;
    bool intersection = false;
    //matrice dei versori normali
    //terza riga: prodotto vettoriale di N1 e N2
    matrixNomalVector.row(2) = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
    rightHandSide[2] = 0;
    Matrix3d solverMatrix =  matrixNomalVector.inverse();
//    solverMatrix << matrixNomalVector(2,2), -matrixNomalVector(1,2), -matrixNomalVector(0,2),
//                    -matrixNomalVector(2,1), matrixNomalVector(1,1), -matrixNomalVector(0,1),
//                    -matrixNomalVector(2,0), -matrixNomalVector(1,0), matrixNomalVector(0,0);

    //vector equation Pi_1: N^t * (x - x_0) or Pi: N^t * x - d = 0
    pointLine = solverMatrix * rightHandSide;
    tangentLine = matrixNomalVector.row(2)/pointLine.squaredNorm();


    if(matrixNomalVector.row(2).squaredNorm() > toleranceParallelism)
    {
        intersectionType = LineIntersection;
        intersection = true;
    }
    else
    {
        if(abs(rightHandSide[0]-rightHandSide[1]) < toleranceIntersection)
        {
            intersectionType = Coplanar;
            intersection = false;
        }
        else
        {
            intersectionType = NoIntersection;
            intersection = false;
        }
    }
    return intersection;
}
