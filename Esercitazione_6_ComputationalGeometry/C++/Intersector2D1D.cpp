#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection =  1.0E-7;
    intersectionType = TypeIntersection::NoIntersection;
}
Intersector2D1D::~Intersector2D1D()
{

}
//Equazione di una retta nello spazio
//r: x_0 + s * t || P1 = P0 + s*(P1 - P0)
//Calcolo coordinate parametriche di un punto note le sue  coordinata curvilinea
Vector3d Intersector2D1D::IntersectionPoint()
{
    return lineOriginPointer[0] + intersectionParametricCoordinate*lineTangentPointer[0];
}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
    return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
    return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    intersectionType = NoIntersection;
    bool intersection = false;
    double parallelism;

    //prodotto scalare del versore normale al piano per versore tangente alla retta
    parallelism = planeNormalPointer[0].dot(lineTangentPointer[0]);

    double check = pow(toleranceIntersection,2) * planeNormalPointer[0].squaredNorm()*(lineTangentPointer[0]).squaredNorm();

    //se parallelismo non verificato, allora piano e retta sono incidenti in un punto
    if(parallelism*parallelism >= check)
    {
        intersectionParametricCoordinate = *planeTranslationPointer - planeNormalPointer[0].dot(lineOriginPointer[0]);
        intersectionParametricCoordinate /= parallelism;
        intersectionType = PointIntersection;
        intersection = true;
        return intersection;
    }
    else
    {
        check = pow(toleranceParallelism,2) * planeNormalPointer[0].squaredNorm()*(lineTangentPointer[0]).squaredNorm();
        parallelism = *planeTranslationPointer - planeNormalPointer[0].dot(lineOriginPointer[0]);
        if(parallelism*parallelism <= check)
        {
            intersectionType = Coplanar;
            intersection = false;
            return intersection;
        }
        else
        {
             intersectionType = NoIntersection;
             intersection = false;
             return intersection;
        }
    }
}
