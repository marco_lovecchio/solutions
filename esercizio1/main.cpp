#include <iostream>
#include <iomanip>
#include <algorithm>

#include <vector>
#include <unordered_map>
#include <map>

#include <chrono>
#include <numeric>

using namespace std;

//Realizzo una funzione che mi permetta di riordinare gli elementi del vector/della map rispetto al secondo elemento della coppia
bool sortbysec(const pair<int, double> &a, const pair<int, double> &b)
{
    return (a.second < b.second);
}

int main()
{
    //Setto la precisione delle cifre decimali dei valori che andrò a inserire all'interno del vector/della map.
    std::cout << std::fixed << std::setprecision(9) << std::left;

    /* ----- Esercizio 1.1, svolto con l'uso di vector ----- */

    std::cout << "PER UN VETTORE DI DIMENSIONI CRESCENTI:\n" << endl;

    //Creo un vettore di pair<int,double> di dimensione crescente.
    for( auto size = 1; size < 100000000; size *= 10)
    {
        std::vector<std::pair<int, double>> vector;

        for( auto i = 0; i < size; i++ )
        {
            //Inizializzo il vettore con numeri generati casualmente.
            vector.push_back(make_pair(rand(),rand()));
        }

        //Faccio partire il conteggio del tempo per riordinare rispetto al primo elemento della coppia.
        auto start1 = std::chrono::high_resolution_clock::now();

        //Riordino il vettore rispetto al primo elemento della coppia.
        std::sort(vector.begin(),vector.end());

        //Fermo il conteggio del tempo.
        auto end1 = std::chrono::high_resolution_clock::now();

        //Analogo procedimento per l'ordinamento rispetto al secondo elemento della coppia, sfruttando la funzione realizzata.
        auto start2 = std::chrono::high_resolution_clock::now();

        std::sort(vector.begin(),vector.end(), sortbysec);

        auto end2 = std::chrono::high_resolution_clock::now();

        //Calcolo il tempo impiegato per entrambi i processi e ne stampo i risultati.
        std::chrono::duration<double> diff1 = end1 - start1;
        std::chrono::duration<double> diff2 = end2 - start2;

        std::cout << "Op. di sort rispetto al primo elemento della coppia: " << diff1.count() << " s" << endl;
        std::cout << "Op. di sort rispetto al secondo elemento della coppia: " << diff2.count() << " s" << endl;
    }

    std::cout << "\n" << endl;

    /* ----- Esercizio 1.2, svolto con l'uso di map ----- */

    std::cout << "PER UNA MAPPA DI DIMENSIONI CRESCENTI: \n" << endl;

    for( auto size = 1; size < 100000000; size *= 10)
    {
        std::unordered_map<int, double> map;

        for( auto i = 0; i < size; i++ )
        {
            map.insert(make_pair(rand(),rand()));
        }

        auto start3 = std::chrono::high_resolution_clock::now();

        //Riordino il vettore rispetto al primo elemento della coppia.
        std::map<int, double> ordered(map.begin(), map.end());

        auto end3 = std::chrono::high_resolution_clock::now();

        //Analogo procedimento per l'ordinamento rispetto al secondo elemento della coppia.
        //Realizzando un vector copia in cui inserire tutti i valori della map per poi riordinarli rispetto al secondo elemento della coppia.
        auto start4 = std::chrono::high_resolution_clock::now();

        std::vector<pair<int, double>> copyVector;

        unordered_map<int, double> :: iterator mapIter;
        for (mapIter = map.begin(); mapIter != map.end(); mapIter++)
        {
           copyVector.push_back(make_pair(mapIter->first, mapIter->second));
        }
        std::sort(copyVector.begin(), copyVector.end(), sortbysec);

        auto end4 = std::chrono::high_resolution_clock::now();

        std::chrono::duration<double> diff3 = end3 - start3;
        std::chrono::duration<double> diff4 = end4 - start4;

        std::cout << "Op. di sort rispetto al primo elemento della coppia: " << diff3.count() << " s" << endl;
        std::cout << "Op. di sort rispetto al secondo elemento della coppia: " << diff4.count() << " s" << endl;
    }

    return 0;
}
