#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt(pow(_a,2)/2 + pow(_b,2)/2);
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;

    for(int i = 0; i < 3; i++)
    {
       double currentEdge;
       //a currentEdge viene assegnato il valore della distanza tra due punti del triangolo, calcolata come norma
       currentEdge = (points[(i+1)%3] - points[i]).ComputeNorm2();
       perimeter += currentEdge;
    }
    return perimeter;
  }

  //Dato un punto, viene costruito un triangolo equilatero di lato = edge con la base parallela all'asse x
  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)//: Triangle(p1, Point(p1.X + edge, p1.Y), Point(p1.X + edge/2, p1.Y + edge*sqrt(3)/2))
  {
      points.push_back(p1);
      points.push_back(Point(p1.X + edge, p1.Y));
      points.push_back(Point(p1.X + edge/2, p1.Y + edge*sqrt(3)/2));
  }


  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      Quadrilateral();
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;

    for(int i = 0; i < 4; i++)
    {
       double currentEdge;
       //a currentEdge viene assegnato il valore della distanza tra due punti consecutivi del quadrilatero, calcolata come norma
       currentEdge = (points[(i+1)%4] - points[i]).ComputeNorm2();
       perimeter += currentEdge;
    }
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height) //: Quadrilateral(p1,Point(p1.X + base, p1.Y),Point())
  {
      Rectangle();
      points.push_back(p1);
      points.push_back(Point(p1.X + base, p1.Y));
      points.push_back(Point(p1.X + base, p1.Y + height));
      points.push_back(Point(p1.X, p1.Y + height));
  }

  Point Point::operator+(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X = X + point.X;
      tempPoint.Y = Y + point.Y;

      return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X = X - point.X;
      tempPoint.Y = Y - point.Y;

      return tempPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;

      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;

      return *this;
  }
}
